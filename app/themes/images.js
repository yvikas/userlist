const Icons = {
  search: require('./resources/search.png'),
  user: require('./resources/workout.jpg'),
  backButton: require('./resources/left.png'),
  menu:require('./resources/menu.png'),
  images: require('./resources/images.jpeg'),
  images1: require('./resources/images1.jpeg'),
  images2: require('./resources/images2.jpeg'),
  images3: require('./resources/images3.jpeg'),
  images4: require('./resources/images4.jpeg'),
  images5: require('./resources/images5.jpeg'),
  images6: require('./resources/images6.jpeg'),
  images7: require('./resources/images7.jpeg'),
  images8: require('./resources/images8.jpeg'),
  images9: require('./resources/images9.jpeg'),
  images10: require('./resources/images10.jpeg'),
  images11: require('./resources/images11.jpeg'),
  images12: require('./resources/images12.jpeg'),
  images13: require('./resources/images13.jpg'),
  images14: require('./resources/images14.jpg'),
  images15: require('./resources/images15.jpg'),
};

module.exports = Icons;
