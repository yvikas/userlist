import React from 'react'
import { TouchableOpacity, Text, StyleSheet, Image } from 'react-native'
import { View } from 'native-base';
import { Images, Fonts, Colors, Metrics } from '../../themes';

const Header = (props) => {
    const { onPressBack, userName } = props
    return (
        <View style={Styles.container}>
            <TouchableOpacity onPress={() => onPressBack()}>
                <Image style={Styles.icon} source={Images.backButton} />
            </TouchableOpacity>
            <Text style={Styles.title}>{userName}</Text>
            <TouchableOpacity>
                <Image style={Styles.icon} source={Images.menu} />
            </TouchableOpacity>
        </View>
    )
}

export default Header

const Styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        alignItems: 'center',
        marginTop: Metrics.screenHeight * 0.0121,
        justifyContent: 'space-between'
    },
    icon: {
        width: Metrics.screenWidth * 0.0533,
        height: Metrics.screenWidth * 0.0533,
    },
    title: {
        fontFamily: Fonts.type.regular,
        color: Colors.white,
        fontSize: Metrics.screenWidth * 0.0533,
    }
})