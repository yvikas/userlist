
import React from 'react'
import { Image, View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Fonts, Colors, Metrics } from '../../themes';

const UserList = (props) => {
    const{userName, fullName, profileImage, onPress} = props
    return (
        <TouchableOpacity activeOpacity={0.7} onPress={()=> onPress()} style={Styles.Container}>
                <Image style={Styles.image} source={profileImage} />
                <View style={Styles.content}>
                    <Text style={Styles.userName}>{userName}</Text>
                    <Text style={Styles.name}>{fullName}</Text>
                </View>
            </TouchableOpacity>
    )
}

export default UserList

const Styles = StyleSheet.create({
    Container: {
        flexDirection: 'row', 
        width:"99%", 
        borderRadius: Metrics.screenWidth * 0.16, 
        borderBottomLeftRadius: Metrics.screenWidth * 0.1333, 
        borderTopLeftRadius: Metrics.screenWidth * 0.1333, 
        alignItems: 'center', 
        height: Metrics.screenHeight * 0.0975, 
        marginVertical: Metrics.screenHeight * 0.0207,  
        shadowRadius: 5,
        shadowColor: Colors.lightBlack,
        shadowOffset: { height: 4, width: 0 },
        shadowOpacity: 0.25,
        backgroundColor: Colors.lightGrey,
        elevation: 5
    },
    content: {
        paddingHorizontal: Metrics.screenWidth * 0.106
    },
    image: {
        width: Metrics.screenWidth * 0.213, 
        height: Metrics.screenWidth * 0.213, 
        borderRadius: Metrics.screenWidth * 0.1066 
    },
    userName:{
        fontFamily:Fonts.type.semiBold
    },
    name:{
        fontFamily:Fonts.type.medium
    }
})