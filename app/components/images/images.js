import React from 'react'
import { Image, View, StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../themes';

const UserImages = (props) => {
    const { userImage } = props
    return (
        <View style={Styles.Container}>
            <Image style={Styles.image} source={userImage} />
        </View>
    )
}

export default UserImages

const Styles = StyleSheet.create({
    Container: {
        flexDirection: 'row', 
        alignItems: 'center', 
        shadowRadius: 5,
        shadowColor: Colors.lightBlack,
        shadowOffset: { height: 1, width: 10 },
        height: Metrics.screenHeight * 0.1341,
        shadowOpacity: 0.25,
        elevation: 9, 
        marginTop: Metrics.screenHeight * 0.0121, 
        justifyContent: 'space-between', 
        paddingHorizontal: Metrics.screenWidth * 0.0266
    },
    image: {
        width: Metrics.screenWidth * 0.293, 
        height: Metrics.screenHeight * 0.1219, 
        borderRadius: Metrics.screenWidth * 0.0213,
    }
})