import React from 'react'
import { Image, View, Text, StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../themes';

const FollowerList = (props) => {
    const { followerImage, name } = props
    return (
        <View style={Styles.Container}>
            <View style={Styles.content}>
                <Image style={Styles.image} source={followerImage} />
            </View>
            <Text style={Styles.name}>{name}</Text>
        </View>
    )
}

export default FollowerList

const Styles = StyleSheet.create({
    Container: {
        paddingHorizontal: Metrics.screenWidth * 0.0266,
    },
    content: {
        shadowRadius: 5,
        shadowColor: Colors.lightBlack,
        shadowOffset: { height: 7, width: 1 },
        shadowOpacity: 0.25,
        elevation: 19,
    },
    image: {
        width: Metrics.screenWidth * 0.186,
        height:  Metrics.screenWidth * 0.186,
        borderRadius: Metrics.screenWidth * 0.093
    },
    name:{
        textAlign: 'center', 
        fontFamily: Fonts.type.regular, 
        fontSize: Metrics.screenWidth * 0.04, 
        marginTop: Metrics.screenHeight * 0.0121 
    }
})