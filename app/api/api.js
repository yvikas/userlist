import { Images } from "../themes";

export const users = [
  {
    "userName": "LCollisond",
    "fullName": "Kip Collison",
    "followers": `12,4200`,
    "following": `98,135`,
    "profileImage": Images.images,
    "followerName": 'Alex',
    "followerImage":Images.user,
    "bannerImage":Images.images1,
    "userImage":Images.images11
  },
  {
    "userName": "RChikanso",
    "fullName": "Chikanso Chima",
    "followers": `12,0340`,
    "following": `54,135`,
    "profileImage": Images.images1,
    "followerName": 'Alan',
    "followerImage":Images.images3,
    "bannerImage":Images.images9,
    "userImage":Images.images5
  },
  {
    "userName": "KAlice",
    "fullName": "Alice Krejčová",
    "followers": `120,0234`,
    "following": `65,135`,
    "profileImage": Images.images2,
    "followerName": 'Mac',
    "followerImage":Images.images10,
    "bannerImage":Images.images8,
    "userImage":Images.images6
  },
  {
    "userName": "DWilliam",
    "fullName": "William Diwedi",
    "followers": `12,0240`,
    "following": `7,135`,
    "profileImage": Images.images3,
    "followerName": 'Adam',
    "followerImage":Images.images15,
    "bannerImage":Images.images10,
    "userImage":Images.images7
  },
  {
    "userName": "CMichal",
    "fullName": "Michal Clark",
    "followers": `13,400`,
    "following": `56,135`,
    "profileImage": Images.images4,
    "followerName": 'Lisa',
    "followerImage":Images.images12,
    "bannerImage":Images.images3,
    "userImage":Images.images8
  },
  {
    "userName": "CJohn",
    "fullName": "John Carter",
    "followers": `1,520`,
    "following": `23,135`,
    "profileImage": Images.images5,
    "followerName": 'Pool',
    "followerImage":Images.images11,
    "bannerImage":Images.images4,
    "userImage":Images.images9
  },
  {
    "userName": "MRyan",
    "fullName": "Ryan Marks",
    "followers": `1,400`,
    "following": `12,135`,
    "profileImage": Images.images6,
    "followerName": 'Ray',
    "followerImage":Images.images10,
    "bannerImage":Images.images12,
    "userImage":Images.images1
  },
  {
    "userName": "MRyan",
    "fullName": "Ryan Markwood",
    "followers": `1,100`,
    "following": `1,435`,
    "profileImage": Images.images7,
    "followerName": 'Wood',
    "followerImage":Images.images9,
    "bannerImage":Images.images13,
    "userImage":Images.images12
  }

]
