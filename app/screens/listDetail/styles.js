import { StyleSheet } from 'react-native';
import { Colors, Fonts, Metrics, } from '../../themes';

export const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: Colors.backgroundColor
    },
    content: {
        marginTop: Metrics.screenHeight * 0.0121
    },
    contentCotainet:{
        paddingBottom: Metrics.screenHeight * 0.0243
    },
    followerTextView: {
        paddingHorizontal: Metrics.screenWidth * 0.0533, 
        marginVertical: Metrics.screenHeight * 0.0304
    },
    followerText: {
        fontFamily: Fonts.type.semiBold, 
        fontSize: Metrics.screenWidth * 0.0453, 
        color: Colors.lightBlack, 
        letterSpacing: 0.71
    },
    bannerImage: {
        width: '100%',
        height: Metrics.screenHeight * 0.304,
    },
    nameView: {
        width: Metrics.screenWidth * 0.24, 
        position: 'absolute', 
        top: Metrics.screenHeight * 0.243, 
        left: Metrics.screenWidth * 0.050, 
        height:  Metrics.screenWidth * 0.24, 
        borderRadius: Metrics.screenWidth * 0.12, 
        alignItems: 'center', 
        justifyContent: 'center',
         borderWidth: 3, 
         borderColor: Colors.white, 
         shadowRadius: 5,
        shadowColor: Colors.lightBlack,
        shadowOffset: { height: 8, width: 0 },
        shadowOpacity: 0.25,
        backgroundColor: Colors.lightGrey,
        elevation: 15
    },
    userImage: {
        width: Metrics.screenWidth * 0.224, 
        height: Metrics.screenWidth * 0.224, 
        borderRadius: Metrics.screenWidth * 0.112, 
    },
    name: {
        alignSelf: 'center', 
        marginTop: Metrics.screenHeight * 0.200, 
        fontFamily: Fonts.type.semiBold, 
        marginLeft: Metrics.screenWidth * 0.026,
        letterSpacing: 1, 
        color: Colors.white, 
        fontSize: Metrics.screenWidth * 0.0453
    },
    followerContainer: {
        paddingHorizontal: Metrics.screenWidth * 0.106, 
        flexDirection: 'row', 
        marginTop: Metrics.screenHeight * 0.00731, 
        alignItems: 'center', 
        justifyContent: 'space-between'
    },
    followerContent: {
        marginLeft: Metrics.screenWidth * 0.24
    },
    followerTitle: {
        fontFamily: Fonts.type.regular
    },
    followerText: {
        fontFamily: Fonts.type.semiBold, 
        letterSpacing: 0.71, 
        fontSize: Metrics.screenWidth * 0.0353
    },
    
})