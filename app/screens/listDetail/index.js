import React, { Component } from 'react'
import { View, FlatList, Text, ImageBackground, Image } from 'react-native'
import { Colors, Images, Fonts } from '../../themes';
import Header from '../../components/header/header';
import { Content } from 'native-base';
import UserImages from '../../components/images/images';
import FollowerList from '../../components/followerList/followerList';
import * as Action from "../list/actions"
import { connect } from 'react-redux';
import { styles } from './styles'

class ListDetail extends Component {

    constructor(props) {
        super(props)
        this.state = {

        };
    }

    componentDidMount() {
        this.props.userList()
    }



    renderItem = ({ item, index }) => {
        return (
            <UserImages
                userImage={item.userImage}
            />
        )
    }


    getList() {
        return (
            <FlatList
                data={this.props.list}
                showsHorizontalScrollIndicator={false}
                renderItem={this.renderItem}
                horizontal
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderFollowerItem({ item }) {
        return (
            <FollowerList
                name={item.followerName}
                followerImage={item.followerImage}
            />
        )
    }

    renderFollower() {
        return (
            <FlatList
                data={this.props.list}
                showsHorizontalScrollIndicator={false}
                renderItem={this.renderFollowerItem}
                horizontal
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderFollowerList() {
        const { followers, following } = this.props.navigation.state.params
        return (
            <View style={styles.followerContainer}>
                <View style={styles.followerContent}>
                    <Text style={styles.followerTitle}>Followers</Text>
                    <Text style={styles.followerText}>{followers}</Text>
                </View>
                <View>
                    <Text style={styles.followerTitle}>Following</Text>
                    <Text style={styles.followerText}>{following}</Text>
                </View>
            </View>
        )
    }

    renderBanner() {
        const { bannerImage, profileImage, fullName, userName } = this.props.navigation.state.params
        return (
            <ImageBackground style={styles.bannerImage} source={bannerImage}>
                <Header userName={userName} onPressBack={()=> this.props.navigation.goBack()} />
                <View style={styles.nameView}>
                    <Image style={styles.userImage} source={profileImage} />
                </View>
                <Text style={styles.name}>{fullName}</Text>
            </ImageBackground>
        )
    }




    render() {
        return (
            <Content style={styles.container} contentContainerStyle={styles.contentCotainet}>
                {this.renderBanner()}
                {this.renderFollowerList()}
                <View style={styles.content}>
                    {this.getList()}
                </View>
                <View style={styles.followerTextView}>
                    <Text style={styles.followerText}>Recently Followers</Text>
                </View>
                {this.renderFollower()}
                <View style={styles.followerTextView}>
                    <Text style={styles.followerText}>Recently Following</Text>
                </View>
                {this.renderFollower()}
            </Content>
        )
    }
}
const mapStateToProps = (state) => ({
    list: state.lists.list
});

const mapDispatchToProps = (dispatch) => ({
    userList: () => dispatch(Action.userList()),
});

const Conatiner = connect(mapStateToProps, mapDispatchToProps)(ListDetail)

export default Conatiner;



