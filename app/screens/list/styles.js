import { StyleSheet } from 'react-native';
import { Colors, Metrics, Fonts, } from '../../themes';

export const styles = StyleSheet.create({
    container: {
        flex:1, 
        paddingHorizontal: Metrics.screenWidth * 0.0533, 
        backgroundColor:Colors.backgroundColor
    },
    commentView: {
        width: '96%', 
        height: Metrics.screenWidth * 0.08, 
        justifyContent: 'space-between', 
        alignSelf:'center',
        paddingVertical:Metrics.screenHeight * 0.0243,
        marginVertical: Metrics.screenHeight * 0.0243,
        backgroundColor: Colors.white, 
        borderRadius: Metrics.screenWidth * 0.0666, 
        flexDirection: 'row', 
        alignItems: 'center'
    },
    commentInput: {
        width: '90%', 
        height: Metrics.screenWidth * 0.16,
        paddingHorizontal: Metrics.screenWidth *0.0266 
    },
    resultNotFoundView:{
        alignItems:'center', 
        justifyContent:'center'
    },
    resultNotFoundText:{
        fontFamily:Fonts.type.semiBold,
        fontSize: Metrics.screenWidth * 0.08, 
        marginTop: Metrics.screenHeight * 0.300
    }
})