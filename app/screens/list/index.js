import React, { Component } from 'react'
import { View, FlatList, TextInput, Text } from 'react-native'
import { styles } from './styles'
import { connect } from 'react-redux';
import * as Action from "./actions"
import UserList from '../../components/userList/useList';
import { Colors } from '../../themes';

class List extends Component {

  constructor(props) {
    super(props)
    this.state = {
      search: ''
    };
  }
  componentDidMount() {
    this.props.userList()
  }


  renderItem = ({ item }) => {

    return (
      <UserList userName={`@` + item.userName}
        fullName={item.fullName}
        onPress={() => this.props.navigation.navigate('ListDetail', item)}
        profileImage={item.profileImage}
      />
    )
  }


  getList() {
    const { list } = this.props
    const { searchedList, search } = this.state
    let data = search == '' ? list : searchedList
    if(data.length){
      return (
        <FlatList
          data={data}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      )
    } else {
      return(
        <View style={styles.resultNotFoundView}>
          <Text style={styles.resultNotFoundText}>Result Not Found</Text>
        </View>
      )
    }
   
  }

  _onSearchTextChange(text) {
    this.setState({ search: text },
      this.searchKeyWord(text)
    )
  }

  searchKeyWord(text) {
    const { list } = this.props
    let data = list.filter(item => {
      return item.fullName.toLowerCase().includes(text.toLowerCase())
    })
    this.setState({ searchedList: data })
  }


  render() {
    return (
      <View style={styles.container}>
        <View style={styles.commentView}>
          <TextInput placeholder="search…" placeholderTextColor={Colors.lightBlack} value={this.state.search} onChangeText={(text) => this._onSearchTextChange(text)} style={styles.commentInput}></TextInput>
        </View>
        {this.getList()}
      </View>
    )
  }
}

const mapStateToProps = (state) => ({
  list: state.lists.list
});

const mapDispatchToProps = (dispatch) => ({
  userList: () => dispatch(Action.userList()),
});

const Conatiner = connect(mapStateToProps, mapDispatchToProps)(List)

export default Conatiner;

