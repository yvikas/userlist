import { users } from '../../api/api';

  export function userList() {
    return {
      type: "USER_LIST",
      payload: users
    }
  }