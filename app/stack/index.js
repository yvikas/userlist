import { createStackNavigator, createAppContainer } from "react-navigation";
import ListScreen from  '../screens/list'
import ListDetailScreen from  '../screens/listDetail'


const AppNavigator = createStackNavigator({

    List: {
        screen: ListScreen
    },
    ListDetail: {
        screen: ListDetailScreen
    },
    

},
    {
        initialRouteName: 'List',
        headerMode: 'none'
    }
);

export default createAppContainer(AppNavigator);